# MiniChat

## Procédures d'installation

1. Installer les dépendances

	npm install

2. Démarer le serveur

	node app

3. Accéder au site web par plusieurs fenêtres

	http://localhost:3001
