const express = require('express')
const app = express()
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const port = 3001;

// Express routing
app.use('/public', express.static('public'));
app.get('/', (req, res)=>{
    res.sendFile(__dirname + '/index.html');
});

// Socket handling
let connectionCounter = 0;
let sockets = new Map();

io.on('connection', (socket)=>{
	socket.name = `User${++connectionCounter}`;
	sockets.set(socket.name, socket);
	console.log(`User ${socket.name} is connected`);
	socket.broadcast.emit('user-event', `${socket.name} is connected`);
	
    socket.on('disconnect', ()=>{
		console.log(`User ${socket.name} is disconnected`);
		socket.broadcast.emit('user-event', `${socket.name} is disconnected`);
		sockets.delete(socket.name);
	});
	
	setTimeout(()=>{
		// Apres 3 secondes, envoyer un message
		socket.emit('delayed-greetings', {
			name: socket.name,
			peerCount: sockets.size
		})
	}, 3000);

	socket.on('can-has-joke', ()=>{
		socket.emit('joke-received', 'Voici une blague!');
		socket.broadcast.emit('joke-received', 'Voici une blague!');
	});

	socket.on('sending-message', (msg)=>{
		socket.broadcast.emit('message-received', `${socket.name}: ${msg}`);
	});
});

// Start server
http.listen(port, () => console.log(`Socket.IO demo listening on port ${port}!`));
