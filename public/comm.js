let socket;

function canhasjoke(){
	socket.emit('can-has-joke');
}

function send(){
	let txtmsg = $("#txtmsg");
	let msg = txtmsg.val();
	$("#output").append(
		$("<p></p>").text(`Je dis: ${msg}`)
	);
	socket.emit('sending-message', msg);
	txtmsg.val("");
	txtmsg.focus();
}

$(function() {
	socket = io();

	socket.on('connect', () => {
		$("#status").text("Connexion établie");
	});

	socket.on('connect_timeout', (timeout) => {
		$("#status").text("Connexion impossible pour l'instant...");
	});

	socket.on('disconnect', (reason) => {
		$("#status").text("Connexion fermée");
	});
	
	socket.on('delayed-greetings', (info)=>{
		console.log(info);
		$("#output").append(
			$("<p></p>").text(`Mon nom est ${info.name} et j'ai ${info.peerCount} ami${info.peerCount > 1 ? 's' : ''}`)
		);
	});

	socket.on('joke-received', (joke)=>{
		$("#output").append(
			$("<p></p>").text(`Joke: ${joke}`)
		);
	});

	socket.on('message-received', (msg)=>{
		$("#output").append(
			$("<p></p>").text(msg)
		);
	});

	socket.on('user-event', (msg)=>{
		$("#output").append(
			$("<p></p>").text(`User event: ${msg}`)
		);
	});
});
